// Vendor Libraries
import Vue from 'vue';
import axios from 'axios';
import moment from 'moment';

// Components
import App from './components/app.vue'

// Vendor objects
window.axios = axios;
window.moment = moment;

// Vue Instance
new Vue ({
  el: '#app',
  render: h => h(App)
})
