var path = require('path')
var webpack = require('webpack')
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var extractStyle = new ExtractTextPlugin('app.min.css');

module.exports = {
  entry: {
          app: './src/main.js',
          vendor: ['vue', 'axios', 'moment'] // reference vendor libraries here to be included in the vendor.min.js output file.

  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: '[name].min.js'
  },
  module: {
    rules: [
      // Vue Components
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Choose one that corresponds with your style/preprocessor choice.
            css:     extractStyle.extract({ loader: 'css-loader',                fallbackLoader: 'vue-style-loader' }),
            //less:    extractStyle.extract({ loader: 'css-loader!less-loader',    fallbackLoader: 'vue-style-loader' }),
            scss:    extractStyle.extract({ loader: 'css-loader!sass-loader',    fallbackLoader: 'vue-style-loader' }),
            //stylus:  extractStyle.extract({ loader: 'css-loader!stylus-loader',  fallbackLoader: 'vue-style-loader' }),
            //postcss: extractStyle.extract({ loader: 'css-loader!postcss-loader', fallbackLoader: 'vue-style-loader' }),
          }
        }
      },
      { test: /\.(scss|sass)$/,       loader: 'sass-loader',  exclude: /node_modules/                }, // SCSS and SASS files
    //{ test: /\.less$/,              loader: 'less-loader',    exclude: /node_modules/                }, // LESS Files
    //{ test: /\.styl$/,              loader: 'stylus-loader',  exclude: /node_modules/                }, // STYLUS Files
      { test: /\.js$/,                loader: 'babel-loader', exclude: /node_modules/                }, // JS Files
      { test: /\.(png|jpg|gif|svg)$/, loader: 'file-loader',  query: { name: '[name].[ext]?[hash]' } }  // Image Files
    ]
  },
  resolve: {
    alias:{
      'vue$' : 'vue/dist/vue.common.js'
    }
  },
   plugins: [
    extractStyle,
    new OptimizeCssAssetsPlugin ({
      assetNameRegExp: /\.min\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: { discardComments: {removeAll: true } },
      canPrint: true}),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor']
    })
  ]
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ])
}
